import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/iview_table',
    name: 'iview_table',
    component: () => import('../views/iview_table.vue')
  },
  {
    path: '/iview_form',
    name: 'iview_form',
    component: () => import('../views/iview_form.vue')
  },
  {
    path: '/iview_button',
    name: 'iview_button',
    component: () => import('../views/iview_button.vue')
  },
  {
    path: '/iview_nav',
    name: 'iview_nav',
    component: () => import('../views/iview_nav.vue')
  },
  {
    path: '/iview_layout',
    name: 'iview_layout',
    component: () => import('../views/iview_layout.vue')
  },
  {
    path: '/iview_other',
    name: 'iview_other',
    component: () => import('../views/iview_other.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
