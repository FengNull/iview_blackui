import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import iView from 'iview';
import './assets/black_3x/index.less';
// import 'iview/dist/styles/iview.css';
// import './assets/black_3x/dist/iview.css';

Vue.use(iView, {
  transfer: true
});

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
