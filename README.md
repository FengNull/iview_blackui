# iview_blackui

#### 介绍
IView暗黑风格主题构建工程，由于IView本身的主题配置方案还是基于白色底色为主，需要配置出暗黑系的主题风格还是比较麻烦的。

本工程默认已经配置好了暗黑色系的风格主题，覆盖IView的所有组件，目前只实现了iview3.x的暗黑风格配置，4.x后续会更新。

工程截图：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1023/152601_068230c8_145187.png "WX20201023-152452.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1023/152616_9c11c664_145187.png "WX20201023-152503.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1023/152625_618f83ed_145187.png "WX20201023-152526.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1023/152635_f34dd8f2_145187.png "WX20201023-152537.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1023/152643_e7a5b398_145187.png "WX20201023-152547.png")

#### 软件架构
- 基于vue-cli构建的基于vue2.x的工程项目
- 使用iview官方推荐的iview-theme工具进行皮肤源码的初始化与打包构建

#### 安装教程

运行如下命令安装相关依赖包并启动项目，如果您只需要构建出皮肤的CSS样式文件，可以直接调到使用说明
```
// 安装依赖包
yarn install

// 启动工程
yarn serve
```
注意：启动过程有可能会因为本地安装的node版本过高（12+）造成启动异常，建议使用nvm安装node 11.15.0，然后切换使用的node版本再进行启动。

#### 使用说明

1.  工程已经默认创建暗黑风格的主题源码，位于目录src/assets下的black_3x文件夹中，如果有其他配置需要，可以直接编辑custom.less文件中的相关变量，

2.  打包主题：命令行工具进入black_3x目录，运行如下命令进行主题的打包
```
iview-theme build -o dist/
```
- 注意：如果在打包中报如下错误
```
fs.js:36
} = primordials;
    ^

ReferenceError: primordials is not defined
    at fs.js:36:5
    at req_ (/Users/huangjian/.config/yarn/global/node_modules/natives/index.js:143:24)
    at Object.req [as require] (/Users/huangjian/.config/yarn/global/node_modules/natives/index.js:55:10)
    at Object.<anonymous> (/Users/huangjian/.config/yarn/global/node_modules/vinyl-fs/node_modules/graceful-fs/fs.js:1:37)
    at Module._compile (internal/modules/cjs/loader.js:1015:30)
    at Object.Module._extensions..js (internal/modules/cjs/loader.js:1035:10)
    at Module.load (internal/modules/cjs/loader.js:879:32)
    at Function.Module._load (internal/modules/cjs/loader.js:724:14)
    at Module.require (internal/modules/cjs/loader.js:903:19)
    at require (internal/modules/cjs/helpers.js:74:18)

```
说明你的node版本过高，可以使用nvm安装11.15.0版本的node（nvm可以进行node的多版本管理，具体使用方法请前往：[https://www.runoob.com/w3cnote/nvm-manager-node-versions.html](https://www.runoob.com/w3cnote/nvm-manager-node-versions.html) 查阅）

#### 参考资料

1.  iview3.x官网：[http://v3.iviewui.com/docs/guide/theme](http://v3.iviewui.com/docs/guide/theme)

#### 友情连接

本项目由PageNow数据可视化技术团队开发，PageNow官网：[http://www.pagenow.cn/](http://www.pagenow.cn/)
